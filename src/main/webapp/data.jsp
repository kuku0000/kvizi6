<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Title</title>
</head>
<body>
<h3>Review Information</h3>

<ul>
    <li>
        <p>Title : <%= request.getParameter("book_title")%>
        </p>
    </li>
    <li>
        <p>Author : <%= request.getParameter("book_author")%>
        </p>
    </li>
    <li>
        <p>Release Year : <%= request.getParameter("book_releaseyear")%>
        </p>
    </li>
    <li>
        <p>Cover Type : <%= request.getParameter("book_covertype")%>
        </p>
    </li>
    <li>
        <p>Genre : <%= request.getParameter("book_genre")%>
        </p>
    </li>
</ul>


</body>
</html>
