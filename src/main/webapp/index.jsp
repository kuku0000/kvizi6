<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<!DOCTYPE html>
<html>
<head>
    <title>JSP - Hello World</title>
</head>
<body>
<h1><%= "Hello World!" %>
</h1>
<br/>

<form action="data.jsp" method="post" target="_blank">
    Title: <input type="text" name="book_title">
    <br>
    Author: <input type="text" name="book_author">
    <br>
    Release year: <input type="text" name="book_releaseyear">
    <br>
    Cover type: <input type="text" name="book_covertype">
    <br>
    Genre: <input type="text" name="book_genre">
    <br>
    <input type="submit" value="Next">
</form>

<%--<% if(request.getParameter("is_successful") != null &&
        request.getParameter("is_successful").equals("yes")) { %>
<p>Information written into DB successfully!</p>
<% } %>--%>

</body>
</html>