<<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<%

    Cookie book_title =  new Cookie("book_title", request.getParameter("book_title"));
    Cookie book_author =  new Cookie("book_author", request.getParameter("book_author"));
    Cookie book_releaseyear =  new Cookie("book_releaseyear", request.getParameter("book_releaseyear"));
    Cookie book_covertype =  new Cookie("book_covertype", request.getParameter("book_covertype"));
    Cookie firsbook_genretName =  new Cookie("book_genre", request.getParameter("book_genre"));

    book_title.setMaxAge(60 * 60 * 24);
    book_author.setMaxAge(60 * 60 * 24);
    book_releaseyear.setMaxAge(60 * 60 * 24);
    book_covertype.setMaxAge(60 * 60 * 24);
    firsbook_genretName.setMaxAge(60 * 60 * 24);

    response.addCookie(book_title);
    response.addCookie(book_author);
    response.addCookie(book_releaseyear);
    response.addCookie(book_covertype);
    response.addCookie(firsbook_genretName);


%>

<html>
<head>
    <title>Title</title>
</head>
<body>

<ul>
    <li>
        <p>First name : <%= request.getParameter("book_title")%>
        </p>
    </li>
    <li>
        <p>Last name : <%= request.getParameter("book_author")%>
        </p>
    </li>
    <li>
        <p>Last name : <%= request.getParameter("book_releaseyear")%>
        </p>
    </li>
    <li>
        <p>Last name : <%= request.getParameter("book_covertype")%>
        </p>
    </li>
    <li>
        <p>Last name : <%= request.getParameter("book_genre")%>
        </p>
    </li>
</ul>

</body>
</html>